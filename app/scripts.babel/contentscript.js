'use strict';

import $ from 'jquery';

const port = chrome.runtime.connect({ name: 'knockknock' });
port.postMessage({ op: 'add', data: [1,2,3] });

port.onMessage.addListener((msg) => {
  if (msg.results) {
    console.log(msg.results);
  }
});
