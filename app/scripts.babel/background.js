'use strict';

import { sum } from './lib/math';

chrome.runtime.onInstalled.addListener(details => {
  console.log('previousVersion', details.previousVersion);
});

chrome.browserAction.setBadgeText({ text: '\'Allo' });

chrome.runtime.onConnect.addListener((port) => {
  console.assert(port.name == 'knockknock');

  port.onMessage.addListener((msg) => {
    if (msg.op == 'add')
      port.postMessage({ results: sum(...msg.data) });
  });
});
